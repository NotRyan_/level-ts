import { LevelGraph } from './LevelGraph';
import { IStroll } from './LevelGraphTyping';
/**
 * A class to detirmine which key within a Stroll query should be populated by the previous result.
 * These classes themselves are only to detirmine population points and are not used otherwise.
 */
export declare class WalkVariable {
    name: string;
    static isWalkVar(val: any): val is WalkVariable;
    constructor(name: string);
}
/**
 * Walks over the given queries matching every requirement and returning populated WalkVariables.
 * WalkVariables are populated between searches with the previous Stroll result.
 * @param this The LevelGraph
 * @param strolls The stroll queries to have the results meet the requiries by.
 */
export default function walk(this: LevelGraph, ...strolls: Array<IStroll<any>>): Promise<any[]>;
//# sourceMappingURL=Walking.d.ts.map