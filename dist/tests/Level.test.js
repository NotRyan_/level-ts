"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Level_1 = __importDefault(require("../Level"));
const path_1 = require("path");
Level_1.default.setRoot('temp_test.local');
// mkdirSync('temp_test.local');
let db;
test('Level database creation', () => {
    db = new Level_1.default('level-db');
    return expect(db).toBeInstanceOf(Level_1.default);
});
test('Invalid Level database creation', () => {
    const creation = () => new Level_1.default({});
    return expect(creation).toThrow(new Error('No valid database instance or path provided'));
});
test('Writing data - .put( test, { test: \'jest\' } )', () => {
    return expect(db.put('test', { test: 'jest' })).resolves.toHaveProperty('test', 'jest');
});
test('Reading data - .get( test )', () => {
    return expect(db.get('test')).resolves.toHaveProperty('test', 'jest');
});
test('Deleting data - .del( test )', () => {
    return expect(db.del('test')).resolves.toBeUndefined();
});
test('Chaining a dataset with chaining', () => {
    return expect(db.chain
        .put('Data1', { test: 'jest' })
        .put('Data2', { test: 'jest' })
        .put('Data3', { test: 'jest' })
        .put('Data4', { test: 'jest' })
        .put('Data5', { test: 'jest2' })
        .put('Data6', { test: 'jest' })
        .put('Data7', { test: 'jest' })
        .finish()).resolves.toBeDefined();
});
test('Dirty Finding - .find( jest2 )', () => {
    return expect(db.find((v) => v.test === 'jest2')).resolves.toMatchObject({ test: 'jest2' });
});
test('Dirty Filter - .filter( jest2 )', () => {
    return expect(db.filter((v) => v.test === 'jest2')).resolves.toMatchObject([{ test: 'jest2' }]);
});
test('Iterate Finding - .iterateFind( jest2 )', () => {
    return expect(db.iterateFind((v) => v.test === 'jest2')).resolves.toMatchObject({ test: 'jest2' });
});
test('Iterate Filter - .iterateFilter( jest2 )', () => {
    return expect(db.iterateFilter((v) => v.test === 'jest2')).resolves.toMatchObject([{ test: 'jest2' }]);
});
test('Entry Exists - .exists( Data4 )', async () => {
    await expect(db.exists('Data4')).resolves.toBe(true);
    return expect(db.exists('Data10')).resolves.toBe(false);
});
test('Entry Merging - .merge( Data4, extra )', async () => {
    await expect(db.merge('Data4', { extra: 'hello' })).resolves.toMatchObject({ test: 'jest', extra: 'hello' });
    return expect(db.get('Data4')).resolves.toMatchObject({ test: 'jest', extra: 'hello' });
});
test('Chaining a dataset with chaining', () => {
    return expect(db.chain
        .get('Data1')
        .get('Data2')
        .finish()).resolves.toMatchObject([
        { test: 'jest' },
        { test: 'jest' }
    ]);
});
test('Streaming dataset', async () => {
    const keys = ['Data2', 'Data3', 'Data4'];
    for (const data of await db.stream({
        gte: 'Data2',
        lte: 'Data4',
    })) {
        expect(keys.includes(data.key)).toBeTruthy();
        expect(data.value).toMatchObject({ test: 'jest' });
    }
});
test('Reading all dataset', async () => {
    return expect(db.all()).resolves.toHaveLength(7);
});
test('Using an already created db inside constructor', async () => {
    const database = require('level')(path_1.resolve('temp_test.local', 'level-db-2'));
    const instance = new Level_1.default(database);
    return expect(instance.put('fuck', {})).resolves.toBeDefined();
});
test('Using the iterator', async () => {
    const iterator = db.iterate();
    const first = await iterator.next();
    expect(first).toBeDefined();
    expect(first.key).toBe('Data1');
    expect(first.value).toMatchObject({ test: 'jest' });
    const second = await iterator.next();
    expect(second).toBeDefined();
    expect(second.key).toBe('Data2');
    iterator.seek('Data4');
    const last = await iterator.next();
    expect(last).toBeDefined();
    expect(last.key).toBe('Data4');
    return iterator.end();
});
//# sourceMappingURL=Level.test.js.map