interface IChainObject<DefaultType> {
    del(key: string): IChainObject<DefaultType>;
    get(key: string): IChainObject<DefaultType>;
    put(key: string, value: DefaultType): IChainObject<DefaultType>;
    finish(): Promise<DefaultType[]>;
}
export default class Level<DefaultType = any> {
    static rootFolder: string;
    static setRoot(path: string): void;
    private DB;
    /**
     * Flavour the previously created database with typing and custom functionalities.
     * @param database Previously created database from custom level package.
     */
    constructor(database: object);
    /**
     * Create a new database instance with the normal level package.
     * @param path The path to the database folder.
     */
    constructor(path: string);
    find(func: (value: DefaultType, ind: number, all: DefaultType[]) => boolean | null | undefined): Promise<DefaultType | undefined>;
    filter(func: (value: DefaultType, ind: number, all: DefaultType[]) => boolean | null | undefined): Promise<DefaultType[]>;
    iterateFind(func: (value: DefaultType, key: string) => boolean | null | undefined): Promise<DefaultType | undefined>;
    iterateFilter(func: (value: DefaultType, key: string) => boolean | null | undefined): Promise<DefaultType[]>;
    exists(key: string): Promise<boolean>;
    get chain(): IChainObject<DefaultType>;
    get(key: string): Promise<DefaultType>;
    put(key: string, value: DefaultType): Promise<DefaultType>;
    del(key: string): Promise<void>;
    merge(key: string, config: Partial<DefaultType>): Promise<DefaultType>;
    all(): Promise<DefaultType[]>;
    stream(opts: Partial<IStreamOptions> & {
        keys?: true;
        values: false;
    }): Promise<string[]>;
    stream(opts: Partial<IStreamOptions> & {
        keys: false;
        values?: true;
    }): Promise<DefaultType[]>;
    stream(opts: Partial<IStreamOptions> & {
        keys?: true;
        values?: true;
    }): Promise<Array<{
        key: string;
        value: DefaultType;
    }>>;
    iterate(opts: Partial<IStreamOptions> & {
        keys?: true;
        values: false;
    }): IIterator<string>;
    iterate(opts: Partial<IStreamOptions> & {
        keys: false;
        values?: true;
    }): IIterator<DefaultType>;
    iterate(opts: Partial<IStreamOptions> & {
        keys?: true;
        values?: true;
    }): IIterator<{
        key: string;
        value: DefaultType;
    }>;
    iterate(): IIterator<{
        key: string;
        value: DefaultType;
    }>;
}
interface IIterator<NextType> {
    [Symbol.asyncIterator](): AsyncIterator<NextType>;
    /**
     * Advance the iterator and yield the entry at that key. The type of key and value depends on the options passed to db.iterator().
     * @note Don't forget to call iterator.end(), even if you received an error!
     */
    next(): Promise<NextType | undefined>;
    /**
     * Seek the iterator to a given key or the closest key. Subsequent calls to iterator.next() will yield entries with keys equal to or larger than target, or equal to or smaller than target if the reverse option passed to db.iterator() was true.
     * @param key Keys equal to or larger than target, or equal to or smaller than target if the reverse option passed.
     * @note If range options like gt were passed to db.iterator() and target does not fall within that range, the iterator will reach its end.
     */
    seek(key: string): void;
    /** Free up underlying resources. If ending failed, it throws an promise error error. */
    end(): Promise<void>;
}
interface IStreamOptions {
    /**define the lower bound of the range to be streamed. Only entries where the key is greater than (or equal to) this option will be included in the range. When reverse=true the order will be reversed, but the entries streamed will be the same. */
    gt: string;
    gte: string;
    /**define the higher bound of the range to be streamed. Only entries where the key is less than (or equal to) this option will be included in the range. When reverse=true the order will be reversed, but the entries streamed will be the same. */
    lt: string;
    lte: string;
    /**Using gte and lte with this value. Defining the required prefix of all entries. */
    all: string;
    /**(default: false) stream entries in reverse order. Beware that due to the way that stores like LevelDB work, a reverse seek can be slower than a forward seek. */
    reverse: boolean;
    /**(default: -1) limit the number of entries collected by this stream. This number represents a maximum number of entries and may not be reached if you get to the end of the range first. A value of -1 means there is no limit. When reverse=true the entries with the highest keys will be returned instead of the lowest keys. */
    limit: number;
    /**(default: true) whether the results should contain keys. If set to true and values set to false then results will simply be keys, rather than objects with a key property. Used internally by the createKeyStream() method. */
    keys: boolean;
    /**(default: true) whether the results should contain values. If set to true and keys set to false then results will simply be values, rather than objects with a value property. Used internally by the createValueStream() method. */
    values: boolean;
}
export {};
//# sourceMappingURL=Level.d.ts.map