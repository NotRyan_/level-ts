import { WalkVariable } from './Walking';
export declare type TripleInput = string;
export interface ITripleBase {
    subject: TripleInput;
    predicate: TripleInput;
    object: TripleInput;
}
export interface ITriple<Predicates extends TripleInput> extends ITripleBase {
    predicate: Predicates;
    [key: string]: any;
}
export interface IGetTriple<Predicates extends TripleInput> extends Partial<ITriple<Predicates>> {
    limit?: number;
    offset?: number;
    reverse?: boolean;
}
export interface IChainObject<Predicates extends TripleInput> {
    put(subject: TripleInput, predicate: Predicates, object: TripleInput): IChainObject<Predicates>;
    put(triple: ITriple<Predicates>): IChainObject<Predicates>;
    del(triple: ITriple<Predicates>): IChainObject<Predicates>;
    get(triple: IGetTriple<Predicates>): IChainObject<Predicates>;
    finish(): Promise<Array<IGetTriple<Predicates>>>;
}
export interface IStroll<Predicates extends TripleInput> {
    subject?: string | WalkVariable;
    predicate?: Predicates | WalkVariable;
    object?: string | WalkVariable;
    filter?: (triple: ITriple<Predicates>) => boolean;
}
export interface ILevelGraphImplementation<P extends TripleInput> {
    chain: IChainObject<P>;
    /**
     * Walks over the given queries matching every requirement and returning populated WalkVariables.
     * WalkVariables are populated between searches with the previous Stroll result.
     * @param this The LevelGraph
     * @param strolls The stroll queries to have the results meet the requiries by.
     */
    walk<ReturnType = any>(...strolls: Array<IStroll<P>>): Promise<ReturnType[]>;
    /**
     * A variable to detirmine which key within a Stroll query should be populated by the previous result.
     */
    v(name: string): WalkVariable;
    /**
     * Create a Triple
     * @param subject The subject (main or most unique value)
     * @param predicate The predicate (what connects the subject and object)
     * @param object The object (a common or less unique value)
     */
    put(subject: TripleInput, predicate: P, object: TripleInput): Promise<void>;
    /**
     * Create a Triple
     * @param triple The Triple object or array
     */
    put(triple: ITriple<P> | Array<ITriple<P>>): Promise<void>;
    /**
     * Deletes a pre-existing triple
     * @param triple The (exact) triple to delete
     */
    del(triple: ITriple<P> | Array<ITriple<P>>): Promise<void>;
    /**
     * Searches for a triple within the database
     * @param triple A Triple with one or two values undefined
     */
    get(triple: IGetTriple<P>): Promise<Array<ITriple<P>>>;
    /**
     * Search the first (alphabetic) triple matching parameters and return the non-given parameter's key.
     * @param subject The subject of the Triple or null
     * @param predicate The predicate or null
     * @param object The object or null
     * @example
     * this.find('ryan', 'owns a', null); // returns first Triple's object (bike)
     * this.find(null, 'owns a', 'bike'); // returns first Triple's subject (ryan)
     */
    find(subject: string | null, predicate: P | null, object?: string | null): Promise<TripleInput | null>;
}
//# sourceMappingURL=LevelGraphTyping.d.ts.map