"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WalkVariable = void 0;
/**
 * A class to detirmine which key within a Stroll query should be populated by the previous result.
 * These classes themselves are only to detirmine population points and are not used otherwise.
 */
class WalkVariable {
    constructor(name) {
        this.name = name;
    }
    static isWalkVar(val) {
        return val instanceof WalkVariable;
    }
}
exports.WalkVariable = WalkVariable;
/**
 * Walks over the given queries matching every requirement and returning populated WalkVariables.
 * WalkVariables are populated between searches with the previous Stroll result.
 * @param this The LevelGraph
 * @param strolls The stroll queries to have the results meet the requiries by.
 */
async function walk(...strolls) {
    // * An initial (empty) context is required.
    let contexts = [{}];
    // ? Stroll down the given queries
    for (const stroll of strolls) {
        contexts = await singleStroll.call(this, stroll, contexts);
        if (contexts.length <= 0)
            return []; // If no contexts are given back, no results satisfy the stroll / query
    }
    return contexts;
}
exports.default = walk;
/**
 * Generate new contexts for current stroll from previous strolls.
 * Uses every given context independently for generating new (more) contexts.
 * Pool every context inside an array for later Strolls.
 * @param this The LevelGraph
 * @param stroll A single stroll query
 * @param contexts All the context from previous strolls
 */
async function singleStroll(stroll, contexts) {
    const newContexts = [];
    for (const context of contexts) {
        const receivedCtx = await singleContext.call(this, stroll, context);
        newContexts.push(...receivedCtx);
    }
    return newContexts;
}
/**
 * Searches the database with the given query using the context to populate the Stroll's WalkVariables.
 * @param this The LevelGraph
 * @param stroll A single Stroll Query
 * @param context The context to use for resolving and populating the search term
 */
async function singleContext(stroll, context = {}) {
    /**
     * The class WalkVariables found within the stroll. [result key]: WalkVar.name
     * @key The key from the result object (subject, predicate or object)
     * @value The walk variable name.
     */
    const walkVars = {};
    // Retreive the search term...
    const searchTerm = getSearchTerm(stroll);
    // Then populate that search term with already context existing values
    // from previous searches. Else everything goes within the search (undefined)
    for (const key in searchTerm) {
        const val = stroll[key];
        if (!WalkVariable.isWalkVar(val))
            continue;
        walkVars[key] = val.name;
        searchTerm[key] = context[val.name];
    }
    const results = await this.get(searchTerm);
    const newContexts = [];
    // Create new contexts with the results.
    for (const result of results) {
        // Check the given filter
        if (stroll.filter && !stroll.filter(result))
            continue;
        const newContext = { ...context };
        // Populate the old context with the returned resolved
        // The context should now exist of stroll walk variables
        for (const [key, name] of Object.entries(walkVars))
            newContext[name] = result[key];
        newContexts.push(newContext);
    }
    return newContexts;
}
/**
 * Return a clean search query without the WalkVariables
 * that can be used by the default LevelGraph.get(...)
 * @param stroll The stroll query
 */
function getSearchTerm(stroll) {
    return {
        object: stroll.object instanceof WalkVariable ? undefined : stroll.object,
        subject: stroll.subject instanceof WalkVariable ? undefined : stroll.subject,
        predicate: stroll.predicate instanceof WalkVariable ? undefined : stroll.predicate,
    };
}
//# sourceMappingURL=Walking.js.map