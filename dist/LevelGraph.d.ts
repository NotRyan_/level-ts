import { WalkVariable } from './Walking';
import { TripleInput, IChainObject, ITriple, IGetTriple, ILevelGraphImplementation, IStroll } from './LevelGraphTyping';
export declare class LevelGraph<StaticPredicates extends TripleInput = string> implements ILevelGraphImplementation<StaticPredicates> {
    get chain(): IChainObject<StaticPredicates>;
    static rootFolder: string;
    static setRoot(path: string): void;
    private DB;
    constructor(path: string);
    v(name: string): WalkVariable;
    walk<T = any>(...strolls: Array<IStroll<StaticPredicates>>): Promise<T[]>;
    put(subject: TripleInput, predicate: StaticPredicates, object: TripleInput): Promise<void>;
    put(triple: ITriple<StaticPredicates> | Array<ITriple<StaticPredicates>>): Promise<void>;
    del(triple: ITriple<StaticPredicates> | Array<ITriple<StaticPredicates>>): Promise<void>;
    get(triple: IGetTriple<StaticPredicates>): Promise<Array<ITriple<StaticPredicates>>>;
    find(subject: string | null, predicate: StaticPredicates | null, object?: string | null): Promise<string | null>;
    /**
     * Generates the most efficient level db key for searching the triple's value.
     * @param triple A insufficient Triple
     */
    private generateSearch;
    /**
     * Generates 3 strings where the Triple will be stored within the underlying Level db
     * @param triple An (exact) Triple
     */
    private generateKeys;
}
//# sourceMappingURL=LevelGraph.d.ts.map