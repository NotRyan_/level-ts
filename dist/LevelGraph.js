"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LevelGraph = void 0;
// tslint:disable: jsdoc-format forin
const path_1 = require("path");
const Level_1 = __importDefault(require("./Level"));
const Walking_1 = __importStar(require("./Walking"));
const instances = {};
class LevelGraph {
    constructor(path) {
        const fullpath = path_1.isAbsolute(path) ? path : path_1.resolve(LevelGraph.rootFolder, path);
        this.DB = instances[fullpath]
            ? instances[fullpath]
            : instances[fullpath] = new Level_1.default(fullpath);
    }
    get chain() {
        // tslint:disable-next-line: no-this-assignment
        const instance = this;
        const promises = [];
        return {
            // @ts-ignore // TODO: Fix this type error
            put(...args) { promises.push(instance.put(...args)); return this; },
            del(...args) { promises.push(instance.del(...args)); return this; },
            get(...args) { promises.push(instance.get(...args)); return this; },
            async finish() { return (await Promise.all(promises)).filter((v) => !!v); },
        };
    }
    static setRoot(path) {
        this.rootFolder = path;
    }
    v(name) {
        return new Walking_1.WalkVariable(name);
    }
    async walk(...strolls) {
        return Walking_1.default.call(this, ...strolls);
    }
    async put(subject, predicate, object) {
        if (Array.isArray(subject))
            return Promise.all(subject.map((t) => this.put(t))).then();
        const triple = !!predicate && !!object && (typeof subject === 'string' || typeof subject === 'number')
            ? { subject, predicate, object }
            : subject;
        const chain = this.DB.chain;
        for (const key of this.generateKeys(triple))
            chain.put(key, triple);
        await chain.finish();
    }
    async del(triple) {
        if (Array.isArray(triple))
            return Promise.all(triple.map((t) => this.del(t))).then();
        const chain = this.DB.chain;
        for (const key of this.generateKeys(triple))
            chain.del(key);
        await chain.finish();
    }
    async get(triple) {
        const prefixKey = this.generateSearch(triple);
        const iterator = this.DB.iterate({ gte: prefixKey, lte: prefixKey + '\xff', keys: false });
        const results = [];
        for await (const result of iterator) {
            if (triple.object && triple.object !== result.object)
                continue;
            if (triple.subject && triple.subject !== result.subject)
                continue;
            if (triple.predicate && triple.predicate !== result.predicate)
                continue;
            results.push(result);
        }
        return results;
    }
    async find(subject, predicate, object) {
        let returnkey;
        if (!subject)
            returnkey = 'subject';
        else if (!predicate)
            returnkey = 'predicate';
        else if (!object)
            returnkey = 'object';
        else
            throw new Error('No nulled argument given. No return specified');
        const prefixKey = this.generateSearch({
            subject: subject || undefined,
            predicate: predicate || undefined,
            object: object || undefined,
        });
        const iterator = this.DB.iterate({ gte: prefixKey, lte: prefixKey + '\xff', keys: false });
        for await (const triple of iterator) {
            if (!!object && triple.object !== object)
                continue;
            if (!!subject && triple.subject !== subject)
                continue;
            if (!!predicate && triple.predicate !== predicate)
                continue;
            await iterator.end();
            return triple[returnkey];
        }
        return null;
    }
    /**
     * Generates the most efficient level db key for searching the triple's value.
     * @param triple A insufficient Triple
     */
    generateSearch(triple) {
        if (triple.predicate)
            return SearchTerms.predicate(triple);
        if (triple.subject)
            return SearchTerms.subject(triple);
        if (triple.object)
            return SearchTerms.object(triple);
    }
    /**
     * Generates 3 strings where the Triple will be stored within the underlying Level db
     * @param triple An (exact) Triple
     */
    generateKeys(triple) {
        return [
            SearchTerms.predicate(triple),
            SearchTerms.subject(triple),
            SearchTerms.object(triple)
        ];
    }
}
exports.LevelGraph = LevelGraph;
LevelGraph.rootFolder = process.env.DATABASES || process.env.DATABASES_ROOT || process.cwd();
/**
 * Try to return the values in order. If the previous value is undefined, don't add undefined but return everything else.
 * This is for generating the search key. It prefixes a letter for triple unique keys.
 * @param order The order to try to return the key in
 * @param input The inputs / values
 */
const putin = (order, ...input) => {
    let key = '';
    for (const index in order) {
        if (!input[index])
            return key;
        else
            key += order[index] + '{' + input[index] + '}';
    }
    return key;
};
/**
 * The three optimized search terms generated for maximum efficiency
 * or something. Just easier to read actually :)
 */
const SearchTerms = {
    predicate: (T) => putin(['P', 'S', 'O'], T.predicate, T.subject, T.object),
    subject: (T) => putin(['S', 'P', 'O'], T.subject, T.predicate, T.object),
    object: (T) => putin(['O', 'P', 'S'], T.object, T.predicate, T.subject),
};
//# sourceMappingURL=LevelGraph.js.map