# Level
> The vanilla level module with typescript functionality.

This package is a vanilla implementation of [level](https://npmjs.com/level) with sprinkled (extra) functionalities that create a more usable and readable codebase.

## Getting started
Creating a database is really simple. You import the default class from `level-ts` and use that as a constructor with a path:
```typescript
import Level from 'level-ts';
const db = new Level<string>('path/to/db');
```
Or you can use a previously created database for only flavouring. Also, don't forget to add the type of the database entry as type argument so that typescript knows what it can expect:
```typescript
import CoreLevel from 'level';
import Flavoured from 'level-ts';
interface IEntry { name: string; value: string; }

const someDb = CoreLevel('path/to/db');
const myDb = new Flavoured<IEntry>(someDb);
```
Now you can use `db` or `myDb` with all the functionalities of `level-ts`!

> ##### Important to note
> There are no callbacks anymore. Everything is now done via promises. See [README.md](/README.md) for more info.

