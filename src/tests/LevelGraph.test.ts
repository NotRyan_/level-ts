import { LevelGraph } from '../LevelGraph';
import { mkdirSync } from 'fs';

LevelGraph.setRoot('temp_test.local');
try { mkdirSync('temp_test.local'); } catch (e) { }
const db = new LevelGraph('levelgraph-db');

it('Writing data', () => {
  return expect(
    db.put({ subject: 'Ryan', predicate: 'is', object: 'cool' })
  ).resolves.toBeUndefined();
});

it('Reading data', () => {
  return expect(
    db.get({ object: 'cool' })
  ).resolves.toMatchObject([
    { subject: 'Ryan', predicate: 'is', object: 'cool' }
  ]);
});

it('Deleting data', async () => {
  await expect(
    db.del({ subject: 'Ryan', predicate: 'is', object: 'cool' })
  ).resolves.toBeUndefined();
  return expect(
    db.get({ subject: 'Ryan', predicate: 'is' })
  ).resolves.toHaveLength(0);
});

it('Chaining actions', () => {
  return expect(
    db.chain
      .del({ subject: 'Ryan', predicate: 'is', object: 'cool' })
      .put({ subject: 'Ryan', predicate: 'has', object: 'juice' })
      .put({ subject: 'Ryan', predicate: 'has', object: 'book' })
      .put({ subject: 'juice', predicate: 'contains', object: 'fruit' })
      .put({ subject: 'juice', predicate: 'contains', object: 'paper' })
      .put({ subject: 'book', predicate: 'contains', object: 'paper' })
      .put({ subject: 'book', predicate: 'contains', object: 'leather' })
      .put({ subject: 'Ryan', predicate: 'lastname', object: 'Awesome' })
      .finish()
  ).resolves.toBeDefined();
});

it('Chaining get', () => {
  return expect(
    db.chain
      .get({ subject: 'book', predicate: 'contains' })
      .get({ subject: 'Ryan', predicate: 'lastname' })
      .finish()
  ).resolves.toMatchObject([[
    { subject: 'book', predicate: 'contains', object: 'leather' },
    { subject: 'book', predicate: 'contains', object: 'paper' }
  ], [
    { subject: 'Ryan', predicate: 'lastname', object: 'Awesome' }
  ]]);
});

it('Walking data', () => {
  return expect(
    db.walk(
      { subject: 'Ryan', predicate: 'has', object: db.v('from') },
      { subject: db.v('from'), predicate: 'contains', object: db.v('contains') }
    )
  ).resolves.toEqual([
    { from: 'book', contains: 'leather' },
    { from: 'book', contains: 'paper' },
    { from: 'juice', contains: 'fruit' },
    { from: 'juice', contains: 'paper' }
  ]);
});

it('Walking data with filter', () => {
  return expect(
    db.walk(
      { subject: 'Ryan', predicate: 'has', object: db.v('from') },
      { subject: db.v('from'), predicate: 'contains', object: db.v('contains'), filter: (t) => t.object !== 'paper' }
    )
  ).resolves.toEqual([
    { from: 'book', contains: 'leather' },
    { from: 'juice', contains: 'fruit' }
  ]);
});

it('Simple find( )', async () => {
  await expect(db.find(null, 'lastname', 'Awesome')).resolves.toBe('Ryan');
  await expect(db.find('Ryan', null, 'Awesome')).resolves.toBe('lastname');
  await expect(db.find('Ryan', 'lastname', null)).resolves.toBe('Awesome');
  await expect(db.find('Blueberry', 'lastname', null)).resolves.toBe(null);
  return expect(db.find('Ryan', 'lastname', 'Awesome')).rejects.toThrow('No nulled argument given. No return specified');
});
