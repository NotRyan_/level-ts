// tslint:disable: jsdoc-format forin
import { isAbsolute, resolve } from 'path';
import Level from './Level';
import walk, { WalkVariable } from './Walking';
import { TripleInput, IChainObject, ITriple, IGetTriple, ITripleBase, ILevelGraphImplementation, IStroll } from './LevelGraphTyping';

const instances: {
  [fullpath: string]: any;
} = {};

export class LevelGraph<StaticPredicates extends TripleInput = string> implements ILevelGraphImplementation<StaticPredicates> {

  public get chain(): IChainObject<StaticPredicates> {
    // tslint:disable-next-line: no-this-assignment
    const instance = this;
    const promises: Array<Promise<any>> = [];
    return {
      // @ts-ignore // TODO: Fix this type error
      put(...args: Parameters<typeof instance.put>) { promises.push(instance.put(...args)); return this; },
      del(...args: Parameters<typeof instance.del>) { promises.push(instance.del(...args)); return this; },
      get(...args: Parameters<typeof instance.get>) { promises.push(instance.get(...args)); return this; },
      async finish() { return (await Promise.all(promises)).filter((v) => !!v); },
    };
  }
  public static rootFolder = process.env.DATABASES || process.env.DATABASES_ROOT || process.cwd();
  public static setRoot(path: string) {
    this.rootFolder = path;
  }

  private DB: Level<ITriple<StaticPredicates>>;
  constructor(path: string) {
    const fullpath = isAbsolute(path) ? path : resolve(LevelGraph.rootFolder, path);
    this.DB = instances[fullpath]
      ? instances[fullpath]
      : instances[fullpath] = new Level(fullpath);
  }

  public v(name: string) {
    return new WalkVariable(name);
  }

  public async walk<T = any>(...strolls: Array<IStroll<StaticPredicates>>): Promise<T[]> {
    return walk.call(this as any, ...strolls);
  }

  public async put(subject: TripleInput, predicate: StaticPredicates, object: TripleInput): Promise<void>;
  public async put(triple: ITriple<StaticPredicates> | Array<ITriple<StaticPredicates>>): Promise<void>;
  public async put(subject: TripleInput | ITriple<StaticPredicates> | Array<ITriple<StaticPredicates>>, predicate?: StaticPredicates, object?: string): Promise<void> {

    if (Array.isArray(subject)) return Promise.all(subject.map((t) => this.put(t))).then();

    const triple = !!predicate && !!object && (typeof subject === 'string' || typeof subject === 'number')
      ? { subject, predicate, object }
      : subject as ITriple<StaticPredicates>;

    const chain = this.DB.chain;
    for (const key of this.generateKeys(triple)) chain.put(key, triple);
    await chain.finish();
  }

  public async del(triple: ITriple<StaticPredicates> | Array<ITriple<StaticPredicates>>): Promise<void> {
    if (Array.isArray(triple)) return Promise.all(triple.map((t) => this.del(t))).then();

    const chain = this.DB.chain;
    for (const key of this.generateKeys(triple)) chain.del(key);
    await chain.finish();
  }

  public async get(triple: IGetTriple<StaticPredicates>): Promise<Array<ITriple<StaticPredicates>>> {
    const prefixKey = this.generateSearch(triple);
    const iterator = this.DB.iterate({ gte: prefixKey, lte: prefixKey + '\xff', keys: false });
    const results: Array<ITriple<StaticPredicates>> = [];
    for await (const result of iterator) {
      if (triple.object && triple.object !== result.object) continue;
      if (triple.subject && triple.subject !== result.subject) continue;
      if (triple.predicate && triple.predicate !== result.predicate) continue;
      results.push(result);
    }
    return results;
  }

  public async find(subject: string | null, predicate: StaticPredicates | null, object?: string | null) {
    let returnkey: keyof ITripleBase;
    if (!subject) returnkey = 'subject';
    else if (!predicate) returnkey = 'predicate';
    else if (!object) returnkey = 'object';
    else throw new Error('No nulled argument given. No return specified');

    const prefixKey = this.generateSearch({
      subject: subject || undefined,
      predicate: predicate || undefined,
      object: object || undefined,
    });

    const iterator = this.DB.iterate({ gte: prefixKey, lte: prefixKey + '\xff', keys: false });
    for await (const triple of iterator) {
      if (!!object && triple.object !== object) continue;
      if (!!subject && triple.subject !== subject) continue;
      if (!!predicate && triple.predicate !== predicate) continue;
      await iterator.end();
      return triple[returnkey];
    }
    return null;
  }

  /**
   * Generates the most efficient level db key for searching the triple's value.
   * @param triple A insufficient Triple
   */
  private generateSearch(triple: IGetTriple<any>) {
    if (triple.predicate) return SearchTerms.predicate(triple);
    if (triple.subject) return SearchTerms.subject(triple);
    if (triple.object) return SearchTerms.object(triple);
  }

  /**
   * Generates 3 strings where the Triple will be stored within the underlying Level db
   * @param triple An (exact) Triple
   */
  private generateKeys(triple: ITriple<string>): [string, string, string] {
    return [
      SearchTerms.predicate(triple),
      SearchTerms.subject(triple),
      SearchTerms.object(triple)
    ];
  }
}

/**
 * Try to return the values in order. If the previous value is undefined, don't add undefined but return everything else.
 * This is for generating the search key. It prefixes a letter for triple unique keys.
 * @param order The order to try to return the key in
 * @param input The inputs / values
 */
const putin = (order: Array<'P' | 'S' | 'O'>, ...input: any) => {
  let key = '';
  for (const index in order) {
    if (!input[index]) return key;
    else key += order[index] + '{' + input[index] + '}';
  }
  return key;
};

/**
 * The three optimized search terms generated for maximum efficiency
 * or something. Just easier to read actually :)
 */
const SearchTerms = {
  predicate: (T: IGetTriple<any>) => putin(['P', 'S', 'O'], T.predicate, T.subject, T.object),
  subject: (T: IGetTriple<any>) => putin(['S', 'P', 'O'], T.subject, T.predicate, T.object),
  object: (T: IGetTriple<any>) => putin(['O', 'P', 'S'], T.object, T.predicate, T.subject),
};
